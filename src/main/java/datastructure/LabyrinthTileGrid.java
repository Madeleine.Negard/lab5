package datastructure;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import cellular.cellstate.ICellState;
import datastructure.Grid;
import labyrinth.ILabyrinth;
import labyrinth.LabyrinthTile;

/**
 * A Grid contains a set of labyrinth tiles
 */
public class Grid<LabyrinthTile> implements IGrid<LabyrinthTile> {
	private final List<LabyrinthTile> cells;
	private final int columns;
	private final int rows;

	public Grid(int rows2, int cols, Object object) {
	}

	/**
	 * Construct a grid with the given dimensions.
	 * 
	 * @param rows
	 * @param columns
	 * @param initElement What the cells should initially hold (possibly null)
	 */public IGrid<LabyrinthTile> LabyrinthTile(int rows, int columns, LabyrinthTile initElement) {
		if (rows <= 0 || columns <= 0) {
			throw new IllegalArgumentException();
		}

		this.columns = columns;
		this.rows = rows;
		cells = new ArrayList<LabyrinthTile>(columns * rows);
		for (int i = 0; i < columns * rows; ++i) {
			cells.add(initElement);
		}
	}

	@Override
	public int numColumns() {
		return columns;
	}

	@Override
	public int numRows() {
		return rows;
	}

	public void checkLocation(Location loc) {
		if (!isOnGrid(loc)) {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public void set(Location loc, LabyrinthTile elem) {
		checkLocation(loc);

		cells.set(coordinateToIndex(loc), elem);
	}

	private int coordinateToIndex(Location loc) {
		return loc.row + loc.col * rows;
	}

	@Override
	public ICellState get(Location loc) {
		checkLocation(loc);

		return cells.get(coordinateToIndex(loc));
	}

	@Override
	public Iterable<Location> locations() {
		return new GridLocationIterator(numRows(), numColumns());
	}

	@Override
	public IGrid<LabyrinthTile> copy() {
		IGrid<LabyrinthTile> newGrid = new Grid<LabyrinthTile> (numRows(), numColumns(), null);

		for (Location loc : this.locations()) {
			newGrid.set(loc, this.get(loc));
		}
		return newGrid;
	}

	@Override
	public boolean isOnGrid(Location loc) {
		if (loc.row < 0 || loc.row >= rows) {
			return false;
		}
		return loc.col >= 0 && loc.col < columns;
	}

	@Override
	public void set(Location loc, ICellState iCellState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void set(Location loc, labyrinth.LabyrinthTile tile) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return null;
	}

}
